# Introduction to Spring Boot

#### Dependency Injection
Injecting of dependencies from the **ApplicationContext** and not needing to instantiate a class

#### Integrating Postgres

We require Spring Data JPA and PostgreSQL JDBC Driver to connect to a Postgres database
```xml
<dependencies>
    <dependency>
        <groupId>org.postgresql</groupId>
        <artifactId>postgresql</artifactId>
        <scope>runtime</scope>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-jpa</artifactId>
    </dependency>
</dependencies>
   
```