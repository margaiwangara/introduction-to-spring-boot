package me.margaiwangara.customer;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Repository("list")
public class CustomerListDataAccessService implements  CustomerDao {
    private static final List<Customer> customers;

    static {
        customers = new ArrayList<>();
        Customer jamila = new Customer(
                2,
                "Jamila",
                "jamila@springboot.com",
                23
        );
        Customer alex = new Customer(
                1,
                "Alex",
                "alex@springboot.com",
                21
        );
        customers.add(alex);
        customers.add(jamila);
    }

    @Override
    public List<Customer> selectAllCustomers() {
        return customers;
    }

    @Override
    public Optional<Customer> selectCustomerById(Integer id) {
        return customers.stream().filter(c -> c.getId().equals(id)).findFirst();
    }

    @Override
    public void insertCustomer(Customer customer) {
        customers.add(customer);
    }

    @Override
    public boolean existsPersonWithEmail(String email) {
        return customers.stream().anyMatch(c -> c.getEmail().equals(email));
    }

    @Override
    public boolean existsPersonWithId(Integer id) {
        return customers.stream().anyMatch(c -> c.getEmail().equals(id));
    }

    @Override
    public void deleteCustomerById(Integer id) {
        customers.stream().filter(c -> c.getId().equals(id)).findFirst().ifPresent(customers::remove);
    }



}
