package me.margaiwangara.customer;

public record CustomerRegistrationRequest(
        String name,
        String email,
        Integer age
) {
}
