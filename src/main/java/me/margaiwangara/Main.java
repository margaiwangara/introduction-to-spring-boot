package me.margaiwangara;

import me.margaiwangara.customer.Customer;
import me.margaiwangara.customer.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication // @ComponentScan @EnableAutoConfiguration, @Configuration
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args); // run spring boot application
    }

    @Bean
    CommandLineRunner runner(CustomerRepository customerRepository) {

        return args -> {
            Customer jamila = new Customer(
                    "Jamila",
                    "jamila@springboot.com",
                    23
            );
            Customer alex = new Customer(
                    "Alex",
                    "alex@springboot.com",
                    21
            );

            List<Customer> customers = List.of(alex, jamila);
            customerRepository.saveAll(customers);
        };
    }

}
